require("gabriel.colorschemes.kanagawa")
require("gabriel.colorschemes.tokyonight")
require("gabriel.colorschemes.nightfox")
-- set colorscheme
vim.cmd('colorscheme nightfox')
