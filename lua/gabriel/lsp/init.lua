local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require "gabriel.lsp.mason"
require("gabriel.lsp.handlers").setup()
require "gabriel.lsp.null-ls"
