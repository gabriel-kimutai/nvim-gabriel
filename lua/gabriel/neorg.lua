require('neorg').setup {
  load = {
    ["core.defaults"] = {},
    ["core.integrations.treesitter"] = {},
    ["core.norg.concealer"] = {},
    ["core.norg.dirman"] = {
      config = {
        workspaces = {
          notes = "~/Documents/notes",
          assignmets = "~/Documents/assignmets",
          prog = "~/Documents/prog/notes",
        }
      }
    },
    ["core.norg.completion"] = {
      config = {
        engine = "nvim-cmp",
        name = "[Neorg]",
      }
    },
  }
}
